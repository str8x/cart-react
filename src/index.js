import React from 'react';
import ReactDOM from 'react-dom';
import Cart from './components/Cart';
import ProductsProvider from './products-hooks';

ReactDOM.render(
  <ProductsProvider>
    <Cart />
  </ProductsProvider>,
  document.getElementById('root')
);
