import React, { createContext, useState, useContext } from 'react';

const ProductsContext = createContext();

export const useProducts = () => useContext(ProductsContext);

const ProductsProvider = ({ children }) => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  /**
   * Delete product from cart.
   */
  const deleteProduct = productId => {
    setProducts(products.filter(product => product.id !== productId));
  }

  /**
   * Set new values for product.
   */
  const changeProductsState = (productId, selectedSku, qty = false) => {
    setProducts(products.map(product => {
      return product.id !== productId
        ? product
        : {
          ...product,
          selectedSku: {
            ...selectedSku,
            qty: qty !== false ? qty : selectedSku.minQty
          }
        };
      }
    ));
  }

  return (
    <ProductsContext.Provider value={{
      products,
      deleteProduct,
      changeProductsState,
      setProducts,
      loading,
      setLoading,
    }}>
      {children}
    </ProductsContext.Provider>
  );
}

export default ProductsProvider;
