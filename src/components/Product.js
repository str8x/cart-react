import React, { useState } from 'react';
import QuantitySelect from "./QuantitySelect";
import './../stylesheet/product.scss';
import { useProducts } from '../products-hooks';

/**
 * Product component class.
 */
const Product = ({ id, skus, title, subtitle, selectedSku }) => {
  const { changeProductsState, deleteProduct } = useProducts();
  const [select, setSelect] = useState('');

  /**
   * Update sku on change of select list.
   */
  const handleChange = (e) => {
    const value = e.target.value;
    const [selectedSku] = skus.filter((sku) => {
      return sku.sku.machineName === value;
    });
    changeProductsState(id, selectedSku);
    setSelect(value);
  }

  /**
   * Render single product.
   */
  return (
    <div className="product">
      <div className="product-left">
        <img src={selectedSku.image} alt=""/>
        <div className="product-content">
          <div className="title">{title}</div>
          <div className="subtitle">{subtitle}</div>
          <div className="product-select">
            <select value={select} onChange={(e) => handleChange(e)}>
              {skus.map((sku, i) =>
                <option key={i} value={sku.sku.machineName}>{sku.sku.label}</option>
              )}
            </select>
          </div>
        </div>
      </div>
      <div className="product-right">
        <QuantitySelect
          min={selectedSku.minQty}
          max={selectedSku.maxQty}
          qty={selectedSku.qty}
          onChangeQty={qty => changeProductsState(id, selectedSku, qty)}
        />
        <div className="price">{`${parseFloat(selectedSku.price * selectedSku.qty).toFixed(2)} €`}</div>
        <button className="product-delete" onClick={() => deleteProduct(id)}>Delete</button>
      </div>
    </div>
  );
}

export default Product;
