import React from 'react';
import './../stylesheet/quantity-select.scss';

/**
 * Quantity select component function.
 */
const QuantitySelect = ({onChangeQty = f => f, qty = 1, min = 1, max = 1}) =>
  (<div className="quantity-select">
    <button disabled={min === qty} onClick={() => onChangeQty(qty - 1)}>-</button>
      <span>{qty}</span>
    <button disabled={max === qty} onClick={() => onChangeQty(qty + 1)}>+</button>
  </div>);

export default QuantitySelect;
