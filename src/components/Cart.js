import React, { useEffect } from 'react';
import ProductList from './ProductList';
import './../stylesheet/cart.scss';
import { useProducts } from '../products-hooks';

/**
 * Cart component class.
 */
const Cart = () => {
  const { setProducts, setLoading, loading } = useProducts();

  useEffect(() => {
    setLoading(true);

    fetch('https://5b2e27bf47942a0014936952.mockapi.io/api/v1/products')
      .then(response => response.json())
      .then(products => {
        products = Array.isArray(products) ? products : [];

        // Set active skus after load from API.
        products.map((product) =>
          product.selectedSku = {...product.skus[0], qty: product.skus[0].minQty}
        );

        setProducts(products);
        setLoading(false);
      })
      .catch(error => {
        setLoading(false);
        console.error(error);
      });
  }, []);

  /**
   * Render cart.
   */
  return (
    <div className="cart">
      <div className="logo">
        <img src={require('./../img/logo.svg')} alt="Adyax"/>
        <p>A Better Experience</p>
      </div>
      <h1>Frond-End Developer</h1>
      <div className="description">
        Adyax’s core values take top priority: we care for our client, and
        we care for our people. Staff and clients work in
        partnership with consistent, transparent communication.
      </div>

      <div className="cart-main">
        {loading ?
          <div className="product-loading">Loading Products</div> :
          <ProductList />
        }
      </div>

      <div className="cart-footer">
        <p>Adyax specializes in working with
          multi-national, multi-brand companies
          on a wide range of customer, client
          and employee-facing solutions.
        </p>
      </div>
    </div>
  );
}

export default Cart;
