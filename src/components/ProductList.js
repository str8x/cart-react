import React from 'react';
import Product from './Product';
import './../stylesheet/product-list.scss';
import { useProducts } from '../products-hooks';

/**
 * Product list component function.
 */
const ProductList = () => {
  let total = 0;
  const { products } = useProducts();

  return (
    <div className="product-list">
      {products.length ?
        products.map((product, i) => {
          total += product.selectedSku.qty * product.selectedSku.price;
          return <Product key={i} {...product} />;
        })
        : <div className="product-empty">No products.</div>
      }
      {total ? <div className="cart-total">{`${parseFloat(total).toFixed(2)} €`}</div> : ''}
    </div>
  );
};

export default ProductList;
